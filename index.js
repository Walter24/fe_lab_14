const http = require('http')
const fs = require('fs')
const url = require('url')


module.exports = () => {

    const logger = name => {
        fs.readFile('./logs.json', (err, content) => {
            const message = {
                message: `New file with name ${name} saved`,
                time: `${Date.now()}`
            }

            if (err) {
                const jsonBody = {
                    logs: [{
                        message
                    }]
                }
                fs.writeFile('./logs.json', JSON.stringify(jsonBody), 'utf8', err => err && console.log(err))
            } else {
                const jsonBody = JSON.parse(content)
                jsonBody.logs.push({
                    message
                })
                fs.writeFile('./logs.json', JSON.stringify(jsonBody), 'utf8', err => err && console.log(err))
            }
        })
    }

    http.createServer(function (request, response) {

        const {
            pathname,
            query
        } = url.parse(request.url, true);

        if (request.method === 'POST') {
            if (pathname === '/file') {
                fs.writeFile(query.filename, query.content, 'utf-8', function (err) {
                    if (err) throw err;
                    console.log('Saved!');
                    logger(query.filename);
                });
                response.writeHead(200, {
                    'Content-type': 'application/json'
                });
            }
            return response.end(JSON.stringify({
                a: 'b'
            }));
        }

        if (request.method === 'GET') {
            if (pathname === '/logs') {
                console.log('pathname - ', pathname);
                fs.readFile('./logs.json', 'utf-8', (err, content) => {
                    if (err) {
                        response.writeHead(400, {
                            'Content-type': 'text/html'
                        });
                        return response.end('No logs has been found');
                    }
                    response.writeHead(200, {
                        'Content-type': 'text/html'
                    });
                    response.write(content);

                    return response.end(content);
                });
                return;
            }

            if (pathname.match(new RegExp(/file/))) {
                const fileName = pathname.split('/').pop();
                fs.readFile(`./${fileName}`, 'utf-8', (err, content) => {
                    if (err) {
                        response.writeHead(400, {
                            'Content-type': 'text/html'
                        });
                        return response.end(`No files with name "${fileName}" found`);
                    }
                    response.writeHead(200, {
                        'Content-type': 'text/html'
                    });
                    response.write(JSON.stringify(content));

                    return response.end();
                });
            }
        }
    }).listen(8080)
}